import React from "react";
import { useLocation } from "react-router-dom";
import { useEffect } from "react";
import SDK from "@ringcentral/sdk";

export default function Test() {
  var rcsdk = new SDK({
    server: process.env.REACT_APP_RC_SERVER_URL,
    clientId: process.env.REACT_APP_RC_CLIENT_ID,
    clientSecret: process.env.REACT_APP_RC_CLIENT_SECRET,
    redirectUri: process.env.REACT_APP_RC_REDIRECT_URI,
  });
  console.log(
    process.env.REACT_APP_RC_SERVER_URL,
    process.env.REACT_APP_RC_CLIENT_ID,
    process.env.REACT_APP_RC_REDIRECT_URI
  );
  //   if (req.session.tokens != undefined) {
  //     var tokensObj = req.session.tokens;
  //     var platform = rcsdk.platform();
  //     platform.auth().setData(tokensObj);
  //     if (platform.loggedIn()) {
  //       if (req.query.api == "extension") {
  //         var endpoint = "/restapi/v1.0/account/~/extension";
  //         return callGetMethod(platform, endpoint, res);
  //       } else if (req.query.api == "extension-call-log") {
  //         var endpoint = "/restapi/v1.0/account/~/extension/~/call-log";
  //         return callGetMethod(platform, endpoint, res);
  //       }
  //       if (req.query.api == "account-call-log") {
  //         var endpoint = "/restapi/v1.0/account/~/call-log";
  //         return callGetMethod(platform, endpoint, res);
  //       } else {
  //         return res.render("test");
  //       }
  //     }
  //   }

  //   function callGetMethod(platform, endpoint, res) {
  //     platform
  //       .get(endpoint)
  //       .then(function (resp) {
  //         res.send(JSON.stringify(resp.json()));
  //       })
  //       .catch(function (e) {
  //         res.send("Error");
  //       });
  //   }

  //   res.redirect("/");
  return (
    <div>
      <b>
        <a href="/logout">Logout</a>
      </b>
      <h2>Call APIs</h2>
      <ul>
        <li>
          <a href="/test?api=extension">Read Extension Info</a>
        </li>
        <li>
          <a href="/test?api=extension-call-log">Read Extension Call Log</a>
        </li>
        <li>
          <a href="/test?api=account-call-log">Read Account Call Log</a>
        </li>
      </ul>
    </div>
  );
}
