import logo from "./logo.svg";
import React from "react";
import "./App.css";
import Home from "./Home";
import Login from "./Login";
import Contacts from "./Contacts";
import Projects from "./Projects";
import OauthCallback from "./OauthCallback";
import Test from "./Test";
import { Routes, Route, Link } from "react-router-dom";

function App() {
  const name = "Divesh";
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/projects" element={<Projects />} />
        <Route path="/oauth2callback" element={<OauthCallback />} />
        <Route path="/test" element={<Test />} />
        <Route path="/contacts" element={<Contacts name={name} />} />
      </Routes>
    </div>
  );
}

export default App;
