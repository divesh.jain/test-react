import React from "react";
import { useLocation } from "react-router-dom";
import { useEffect } from "react";
import SDK from "@ringcentral/sdk";

function OauthCallback() {
  var rcsdk = new SDK({
    server: process.env.REACT_APP_RC_REDIRECT_URL,
    clientId: process.env.REACT_APP_RC_CLIENT_ID,
    clientSecret: process.env.REACT_APP_RC_CLIENT_SECRET,
    redirectUri: process.env.REACT_APP_RC_REDIRECT_URI,
  });
  //   console.log(process.env.REACT_APP_RC_REDIRECT_URL);
  const search = useLocation().search;
  const code_ = new URLSearchParams(search).get("code");
  if (code_) {
    console.log(code_);
    // console.log(process.env.REACT_APP_RC_REDIRECT_URL);
    var platform = rcsdk.platform();
    platform
      .login({
        code: code_,
        redirectUri: process.env.REACT_APP_RC_REDIRECT_URL,
      })
      .then(function (token) {
        // req.session.tokens = token.json();
        console.log(token.json());
        sessionStorage.setItem("token", token.json());
        // res.send("login success");
      })
      .catch(function (e) {
        // res.send("Login error " + e);
        console.log("Error occurred", e);
      });
  } else {
    console.log("No auth code");
  }
  return <div>OauthCallback</div>;
}

export default OauthCallback;
