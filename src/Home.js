import React from "react";

function Home() {
  return (
    <div>
      <h2>Welcome to the home page</h2>
      <button
        onClick={() => {
          window.location = "/login";
        }}
      >
        Click to Login to Ringcentral
      </button>
    </div>
  );
}

export default Home;
