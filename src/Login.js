import React from "react";
import { useEffect } from "react";
import SDK from "@ringcentral/sdk";

function Login() {
  useEffect(() => {
    var rcsdk = new SDK({
      server: process.env.REACT_APP_RC_SERVER_URL,
      clientId: process.env.REACT_APP_RC_CLIENT_ID,
      clientSecret: process.env.REACT_APP_RC_CLIENT_SECRET,
      redirectUri: process.env.REACT_APP_RC_REDIRECT_URL,
    });
    var platform = rcsdk.platform();
    var authorize_uri = platform.loginUrl({
      brandId: "",
      redirectUri: process.env.REACT_APP_RC_REDIRECT_URL,
    });
    var redirect_uri = process.env.REACT_APP_RC_REDIRECT_URL;
    var config = {
      authUri: authorize_uri,
      redirectUri: redirect_uri,
    };
    console.log(config);
    console.log("The server url is " + process.env.REACT_APP_RC_SERVER_URL);
    console.log("The client id  is " + process.env.REACT_APP_RC_CLIENT_ID);
    console.log(
      "The client secret is " + process.env.REACT_APP_RC_CLIENT_SECRET
    );
    console.log("The redirect url is " + process.env.REACT_APP_RC_REDIRECT_URL);
    var OAuthCode = function (config) {
      this.config = config;
      this.loginPopup = function () {
        this.loginPopupUri(this.config["authUri"], this.config["redirectUri"]);
      };
      this.loginPopupUri = function (authUri, redirectUri) {
        var win = window.open(authUri, "windowname1", "width=800, height=600");
        var pollOAuth = window.setInterval(function () {
          try {
            if (win.document.URL.indexOf(redirectUri) !== -1) {
              window.clearInterval(pollOAuth);
              // win.close();
              window.location.href = "test";
            }
          } catch (e) {
            console.log(e);
          }
        }, 100);
      };
    };
    console.log(authorize_uri, redirect_uri);
    var oauth = new OAuthCode(config);
    oauth.loginPopup();
  });

  return <div>Welcome to the Login page</div>;
}

export default Login;
